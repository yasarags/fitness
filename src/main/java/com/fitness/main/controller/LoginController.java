package com.fitness.main.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.fitness.main.model.Admin;
import com.fitness.main.model.Userdetails;
import com.fitness.main.services.LoginService;
import com.fitness.main.services.UserdetailsService;

@Controller
public class LoginController {

	
	@Autowired
	private UserdetailsService userDetailService;
	
	@Autowired
	private LoginService loginService;
	
	@GetMapping("/")
	public String homeURL() {
		return "index";
	}
	
	@RequestMapping(value = "/home", method = RequestMethod.POST)
	public String adminLogin(Admin admin,Model model) {
		boolean process = loginService.get(admin);
		if(process)
		{
		return "redirect:/adminhome";
		}
		return "redirect:/";
	}
	
	/*
	@RequestMapping(value = "/home", method = RequestMethod.POST)
	public String userLogin(User user,Model model) {
		boolean process = loginService.getByUser(user);
		if(process)
		{
		return "redirect:/adminhome";
		}
		return "redirect:/";
	}
	*/
	
	@RequestMapping(value = "/adminhome")
	public String adminHome(Model model) {
		
		List<Userdetails> listProducts = userDetailService.listAll();
		model.addAttribute("listUsers", listProducts);
		return "home";
		
	}
	
	@GetMapping("/newuser")
	public String newUserByAdmin() {
		return "addUserByAdmin";
	}
	
	
	@RequestMapping(value = "/newuserbyadmin", method = RequestMethod.POST)
	public String saveNewUser( Userdetails userdetails ) {
		
		userDetailService.save(userdetails);
		
		return "redirect:/adminhome";
	}
	
	@RequestMapping("/edit/{id}")
	public ModelAndView showEditProductPage(@PathVariable(name = "id") int id) {
		ModelAndView mav = new ModelAndView("edit_user");
		Userdetails user = userDetailService.get(id);
		mav.addObject("user", user);
		
		return mav;
	}
	
	@RequestMapping(value = "/updateuser", method = RequestMethod.POST)
	public String updateUser( Userdetails userdetails) {

		userDetailService.save(userdetails);
	    
	    return "redirect:/adminhome";
	}
	
	@RequestMapping("/delete/{id}")
	public String deleteProduct(@PathVariable(name = "id") int id) {
		userDetailService.delete(id);
		return "redirect:/adminhome";		
	}
	
	@RequestMapping("/logout")
	public String logout() {
		return "redirect:/";		
	}
}
