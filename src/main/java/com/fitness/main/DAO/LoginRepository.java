package com.fitness.main.DAO;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fitness.main.model.Admin;

public interface LoginRepository extends JpaRepository<Admin, Long> {
	Admin findByUsername(String username);
}
