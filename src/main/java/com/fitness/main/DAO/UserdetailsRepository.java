package com.fitness.main.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.fitness.main.model.Userdetails;


@Repository
public interface UserdetailsRepository extends JpaRepository<Userdetails, Long> {

}
