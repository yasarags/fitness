package com.fitness.main.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fitness.main.DAO.UserdetailsRepository;
import com.fitness.main.model.Userdetails;

@Service
@Transactional
public class UserdetailsService {

	@Autowired
	private UserdetailsRepository userDetailrepo;
	
	public List<Userdetails> listAll() {
		return userDetailrepo.findAll();
	}
	
	public void save(Userdetails userdetails) {
		userDetailrepo.save(userdetails);
	}
	
	public Userdetails get(long id) {
		return userDetailrepo.findById(id).get();
	}
	
	public Userdetails update(Userdetails userdetails) {
		userDetailrepo.save(userdetails);
		return userdetails;
	}
	
	public void delete(long id) {
		userDetailrepo.deleteById(id);
	}
}
