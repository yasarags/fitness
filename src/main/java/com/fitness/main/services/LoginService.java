package com.fitness.main.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fitness.main.DAO.LoginRepository;
import com.fitness.main.model.Admin;

@Service
@Transactional
public class LoginService {

	@Autowired
	private LoginRepository loginService;
	
//	@Autowired
//	private UserRepository userRepository;
	
	/*public boolean getByUser(User user) {
		
		boolean isTrue = false;
		User loginResult = userRepository.findByUsername(user.getUsername());
		if(loginResult.getPassword().equals(user.getPassword()))
		{
			isTrue=true;
		}
		return isTrue;
	}*/

	public boolean get(Admin admin) {
		
		boolean isTrue = false;
		Admin loginResult = loginService.findByUsername(admin.getUsername());
		if(loginResult.getPassword().equals(admin.getPassword()))
		{
			isTrue=true;
		}
		return isTrue;
	}
}
